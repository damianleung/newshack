'use strict';

var app = angular.module('newshack', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'newshack.services', 'wu.masonry'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });

    $urlRouterProvider.otherwise('/');
  })
  .filter("image", function(ImageService) {
	  return function(url){
	  	ImageService.getImageForURL(url).then(function(resp){
	  		return resp;
	  	});
	  	//return url;
	  }
})
  .directive("newsSource", function(ImageService) {
  return {
    //template: "<div style='background-image: url({{url}})'></div>",
    scope: {
      siteUrl: "="
    },
    link: function(scope, element, attrs) {
    	//console.log(scope.siteUrl);
    	ImageService.getImageForURL(scope.siteUrl).then(function(resp){
    		//console.log(resp);
	  		//scope.url = resp;
	  		attrs.$set('style', 'background-image:url('+resp+');');
	  	});
    }
  }
});