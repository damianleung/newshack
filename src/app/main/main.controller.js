'use strict';

angular.module('newshack')
    .controller('MainCtrl', function ($scope, $http, RSSList, ImageService, $timeout) {
      $scope.feeds = RSSList.get();
      $scope.currentCountry = 'all';
      $scope.Math = window.Math;
      $scope.changeCountry = function (n) {
        $scope.currentCountry = n;
        $scope.$emit('wu.masonry.reloaded');
      };
      $scope.sidebarOpen = true;
      $scope.toggleSidebar = function() {
        console.log($scope.sidebarOpen);
        $scope.sidebarOpen = !$scope.sidebarOpen;
        console.log(!$scope.sidebarOpen);
      };
      $scope.getRandomShare = function(){
        return Math.round(Math.random()*1000);
      };
    }).filter('unique', function () {

      return function (items, filterOn) {

        if (filterOn === false) {
          return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
          var hashCheck = {}, newItems = [];

          var extractValueToCompare = function (item) {
            if (angular.isObject(item) && angular.isString(filterOn)) {
              return item[filterOn];
            } else {
              return item;
            }
          };

          angular.forEach(items, function (item) {
            var valueToCheck, isDuplicate = false;

            for (var i = 0; i < newItems.length; i++) {
              if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                isDuplicate = true;
                break;
              }
            }
            if (!isDuplicate) {
              newItems.push(item);
            }

          });
          items = newItems;
        }
        return items;
      };
    });
