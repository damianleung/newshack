'use strict';

angular.module('newshack.services', ['ngResource'])
  .factory('FeedLoader', function ($resource) {
    return $resource('http://ajax.googleapis.com/ajax/services/feed/load', {}, {
      fetch: { method: 'JSONP', params: {v: '1.0', callback: 'JSON_CALLBACK'} }
    });
  })
 .factory('ImageService', ['$http', '$q', function ($http, $q) {
    return{
      getImageForURL: function(url){
          var deferred = $q.defer();
          var promises = [];
          var response = "";
          if(url===undefined){
            return deferred.promise;
          }
          var resolver = new ImageResolver({ requestPlugin : function ( request ) {
            request.url = "http://www.inertie.org/ba-simple-proxy.php?mode=native&url=" + encodeURIComponent( request.url );
            return request;
          } });
          resolver.register(new ImageResolver.FileExtension());
          resolver.register(new ImageResolver.MimeType());
          resolver.register(new ImageResolver.Opengraph());
          resolver.register(new ImageResolver.Webpage());

          resolver.resolve(url, function(result){
            if ( result ) {
              response = result.image;
              deferred.resolve(result.image);
            }
          });

        return deferred.promise;
      }
    }
  }])
  .service('RSSList', function ($rootScope, $q, FeedLoader) {
    var feeds = [];
      this.get = function() {
        var feedSources = [
          {title: 'The Guardian', country: 'uk', url: 'http://www.theguardian.com/uk/rss'},
          {title: 'De Redactie 2', country: 'belgium', url: 'http://deredactie.be/cm/vrtnieuws?mode=atom'},
          {title: '', country: 'uk', url: 'http://feeds.bbci.co.uk/news/rss.xml?edition=int'},
          {title: '', country: 'france', url: 'http://www.dailymail.co.uk/home/index.rss'},
          {title: '', country: 'france', url: 'http://liberation.fr.feedsportal.com/c/32268/fe.ed/rss.liberation.fr/rss/latest/'},
          {title: '', country: 'netherlands', url: 'http://www.nrc.nl/rss.php'},
          {title: '', country: 'spain', url: 'http://ep00.epimg.net/rss/elpais/portada.xml'},
          {title: '', country: 'italy', url: 'http://www.repubblica.it/rss/homepage/rss2.0.xml'},
          {title: '', country: 'portugal', url: 'http://feeds.jn.pt/JN-ULTIMAS'},
          {title: '', country: 'zwitserland', url: 'http://feeds.jn.pt/JN-ULTIMAS'},
          {title: '', country: 'germany', url: 'http://www.spiegel.de/schlagzeilen/tops/index.rss'},
          {title: '', country: 'germany', url: 'http://rss.bild.de/bild-home.xml'},
          {title: '', country: 'ireland', url: 'https://www.irishtimes.com/cmlink/news-1.1319192'},
          {title: '', country: 'denmark', url: 'http://www.bt.dk/bt/seneste/rss'},
          {title: '', country: 'norway', url: 'http://www.aftenposten.no/rss/'},
          {title: '', country: 'sweden', url: 'http://www.aftonbladet.se/rss.xml'},
          {title: '', country: 'finland', url: 'http://www.iltalehti.fi/rss.xml'},
          {title: '', country: 'iceland', url: 'http://www.mbl.is/feeds/fp/'},
          {title: '', country: 'ostenreich', url: 'http://rss.orf.at/news.xml'},
          {title: '', country: 'tsjechië', url: 'http://www.novinky.cz/rss2/'},
          {title: '', country: 'hongary', url: 'http://444.hu/feed/'},
          {title: '', country: 'slovakije', url: 'http://www.topky.sk/rss/11/Spravy_-_Zahranicne.rss'},
          {title: '', country: 'bulgary', url: 'http://offnews.bg/rss/all'},
          {title: '', country: 'russia', url: 'http://ria.ru/export/rss2/world/index.xml'},
          {title: '', country: 'grece', url: 'http://www.kathimerini.gr/rss'},
          {title: '', country: 'slovenia', url: 'http://www.24ur.com/rss'},
          {title: '', country: 'albania', url: 'http://www.balkanweb.com/site/feed/'},
          {title: '', country: 'bosnia', url: 'http://www.avaz.ba/rss/svevijesti'},
          {title: '', country: 'croatia', url: 'http://www.index.hr/najnovije/rss.ashx'},
          {title: '', country: 'macedonia', url: 'http://feeds.feedburner.com/kurir/makedonija?format=xml'},
          {title: '', country: 'romania', url: 'http://adevarul.ro/rss/'},
          {title: '', country: 'letland', url: 'http://vesti.lv/feed'},
          {title: '', country: 'belgium', url: 'http://www.standaard.be/rss/section'},
          {title: '', country: 'belgium', url: 'http://www.demorgen.be/nieuws/rss.xml'},
          {title: '', country: 'belgium', url: 'http://www.hln.be/rss.xml'},
          {title: '', country: 'belgium', url: 'http://feeds.nieuwsblad.be/nieuws/snelnieuws?format=xml'},
          {title: '', country: 'belgium', url: 'http://www.hbvl.be/rss/section/d1618839-f921-43cc-af6a-a2b200a962dc'},
          {title: '', country: 'belgium', url: 'http://www.gva.be/rss/section/ca750cdf-3d1e-4621-90ef-a3260118e21c'},
          {title: '', country: 'belgium', url: 'http://www.knack.be/nieuws/feed.rss'},
          {title: '', country: 'belgium', url: 'http://www.tijd.be/rss/top_stories.xml'},
          {title: '', country: 'belgium', url: 'http://newsmonkey.be/auto/rss/main.rss'},
          {title: '', country: 'belgium', url: 'http://www.lesoir.be/feed/Actualit%C3%A9/Fil%20Info/destination_principale_block'},
          {title: '', country: 'belgium', url: 'http://www.lalibre.be/rss.xml'},
          {title: '', country: 'belgium', url: 'http://www.lavenir.net/rss.aspx?foto=1&intro=1&section=info&info=df156511-c24f-4f21-81c3-a5d439a9cf4b'},
          {title: '', country: 'belgium', url: 'http://www.lecho.be/rss/top_stories.xml'},
          {title: '', country: 'belgium', url: 'http://www.dhnet.be/rss/section/actu.xml'},
          {title: '', country: 'belgium', url: 'http://www.levif.be/actualite/feed.rss'},
          {title: '', country: 'belgium', url: 'http://www.7sur7.be/rss.xml'},
          {title: '', country: 'belgium', url: 'http://www.vandaag.be/xml/vandaagbe-algemeen.xml'},
          {title: '', country: 'belgium', url: 'http://kw.knack.be/west-vlaanderen/feed.rss'},
          {title: '', country: 'belgium', url: 'http://feeds.feedburner.com/apache/feed/voorpagina'},
          {title: '', country: 'france', url: 'http://www.franceinfo.fr/rss.xml'}
        ];
        if (feeds.length === 0) {
          for (var i=0; i<feedSources.length; i++) {
            (function(index) {
            FeedLoader.fetch({q: feedSources[index].url, num: 2}, {}, function (data) {
                  var feed = data.responseData.feed;
                  feed.country = feedSources[index].country;
                  feeds.push(feed);
            });
            })(i);
          }
        }

        return feeds;
      };
  });