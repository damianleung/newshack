var resolver = new ImageResolver({ requestPlugin : proxify });
resolver.register(new ImageResolver.FileExtension());
resolver.register(new ImageResolver.MimeType());
resolver.register(new ImageResolver.Opengraph());
resolver.register(new ImageResolver.Webpage());

function proxify( request ) {
  request.url = "http://www.inertie.org/ba-simple-proxy.php?mode=native&url=" + encodeURIComponent( request.url );
  return request;
}

function getMainImage( url ){
  var img_url = 'd';

  resolver.resolve(url, function(result){
    if ( result ) {
      img_url = result.image;
    }
  });

  return img_url;
}

jQuery(function() {

  $('.toggle').on('click', function(){
    console.log('clicked');
    var sidebar = $('.sidebar');
    if (sidebar.hasClass('slide-in')) {
      $('.sidebar').addClass('slide-in');
    } else {
      $('.sidebar').removeClass('slide-in');
    }
  });

});